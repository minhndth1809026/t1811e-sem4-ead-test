package com.example.eadexam.controller;

import com.example.eadexam.entity.Candidate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CandidateController {

    @GetMapping("/")
    public String index(Model model) {
        RestTemplate restTemplate = new RestTemplate();
        List<Candidate> candidateList = restTemplate.getForObject("http://localhost:1209/api/v1/candidates/", List.class);
        System.out.println(candidateList);
        model.addAttribute("candidateList", candidateList);
        return "candidates";
    }
}

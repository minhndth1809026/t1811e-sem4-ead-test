<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Candidate List</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>List</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Address</th>
            <th scope="col">Date of birth</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${candidateList}" var="ca">
            <tr>
                <td scope="row">${ca.id}</td>
                <td>${ca.name}</td>
                <td>${ca.address}</td>
                <td>${ca.dob}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>

</html>